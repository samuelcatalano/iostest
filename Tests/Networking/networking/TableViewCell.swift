//
//  TableViewCell.swift
//  networking
//
//  Created by Samuel D. N. Catalano on 1/28/16.
//  Copyright © 2016 Samuel Catalano. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class TableViewCell: UITableViewCell {
    
    @IBOutlet var title: UILabel?
    @IBOutlet var img: UIImageView?
    var itsDone : Bool?

    override func awakeFromNib( ) {
        super.awakeFromNib( )
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}