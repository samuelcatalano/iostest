//
//  MainViewController.swift
//  networking
//
//  Created by Samuel Catalano on 1/28/16.
//  Copyright © 2016 Samuel Catalano. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var segmentedControll : UISegmentedControl?
    @IBOutlet weak var table: UITableView!
    
    var array : NSMutableArray?
    
    var arrayBase = Array<NSDictionary>()
    var arrayTodo = Array<NSDictionary>()
    var arrayDone = Array<NSDictionary>()
    var arrayAll = Array<NSDictionary>()
    
    init(array:  NSMutableArray) {
        super.init(nibName: "MainViewController", bundle: nil)
        self.array = array;
        
        arrayAll = array as NSArray as! [NSDictionary]
        arrayBase = arrayAll
        arrayTodo.append((self.array?.objectAtIndex(0))! as! NSDictionary)
        arrayTodo.append((self.array?.objectAtIndex(2))! as! NSDictionary)
        arrayDone.append((self.array?.objectAtIndex(1))! as! NSDictionary)
        arrayDone.append((self.array?.objectAtIndex(3))! as! NSDictionary)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad( ) {
        super.viewDidLoad( )
        view.backgroundColor = UIColor.whiteColor( )
        
        // tableView config
        let nibName = UINib(nibName: "TableViewCell", bundle:nil)
        self.table.registerNib(nibName, forCellReuseIdentifier: "Cell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! TableViewCell
        let dic : NSDictionary?
        
        dic  = arrayAll[indexPath.row]
        
        let url = NSURL(string : (dic!["image"] as? String)!)
        let data = NSData(contentsOfURL: url!)
        
        cell.title?.text = dic!["task"] as? String
        cell.img?.image = UIImage(data:data!)
        cell.itsDone = dic!["done"] as? Bool
        
        return cell
    }
    
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return (arrayAll.count)
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 270
    }
    
    @IBAction func segmentedControll(sender: AnyObject) {
        if(segmentedControll!.selectedSegmentIndex == 0) {
            arrayAll = arrayTodo
            self.table.reloadData( )
        }
        else if(segmentedControll!.selectedSegmentIndex == 1) {
            arrayAll = arrayDone
            self.table.reloadData( )
        }
        else if(segmentedControll!.selectedSegmentIndex == 2) {
            arrayAll = arrayBase
            self.table.reloadData( )
        }
    }
}